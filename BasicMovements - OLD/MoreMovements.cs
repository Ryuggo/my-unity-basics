using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BasicLibrary.Movements {

    public class MoreMovements : BasicMovements{
#region Variables
        [Header("Dodging")]
        [SerializeField] private bool _allowDodging = true;
        [SerializeField] protected float _dodgeForce = 5;
        [SerializeField] private bool _canMoveWhenDodging = false;
        protected bool _isDodging = false;
        
        [Header("Crouching")]
        [SerializeField] private bool _allowCrouching = true;
        [SerializeField] protected float _crouchSpeed = 3;
        [SerializeField] private bool _holdCrouch = false;
        protected bool _isCrouching = false;

		private int _animCrouch = Animator.StringToHash("IsCrouching");
		private int _animDodge = Animator.StringToHash("IsDodging");
#endregion


        protected override void InputEvent() {
            base.InputEvent();

            // Subscribe to the Inputs Events
            _inputMaster.Gameplay.Crouch.performed += _ => OnCrouch();
            _inputMaster.Gameplay.Crouch.canceled += _ => OnStopCrouch();
            
            _inputMaster.Gameplay.Dodge.performed += _ => StartCoroutine(OnDodge());
        }

        protected override float AdjustSpeed() { return _isSprinting ? _sprintSpeed : _isDodging ? _dodgeForce : !_isWalking ? _jogSpeed : _isCrouching ? _crouchSpeed : _walkSpeed; }
        protected override bool MoveOption() { return _isDodging && !_canMoveWhenDodging ? false : true; }
        protected override void MoveTransitionAnim() { _animator.SetFloat(_animMoveTrans, _isCrouching && _inputMovement == Vector2.zero ? 0 : _rigidBody.velocity.magnitude / (_isSprinting ? _sprintSpeed / 2 : !_isWalking ? _jogSpeed : _walkSpeed * 2));}


#region Movements
        private void OnCrouch() {
            if(_inputMovement == Vector2.zero) Crouch();
        }
        protected void OnStopCrouch() {
            if(_inputMovement == Vector2.zero)
                if(_holdCrouch && _isCrouching) {
                    _isCrouching = false;
                    _animator.SetBool(_animCrouch, false);
                }
        }

        protected virtual void Crouch() {
            if(_allowCrouching) {
                if(!_holdCrouch)
                    _isCrouching = _isCrouching ? false : true;
                else
                    _isCrouching = true;

                _animator.SetBool(_animCrouch, _isCrouching);
            }
        }
        protected override void SprintAnim() { if(_isSprinting) _animator.SetBool(_animCrouch, _isCrouching = false); }
        protected override IEnumerator JumpAnim() {
            //_animator.SetBool(_animGrounded, false);
            _animator.SetTrigger(_animJump);
            yield return new WaitForSeconds(0.1f);
            _animator.SetBool(_animCrouch, _isCrouching = false);
        }


        protected virtual IEnumerator OnDodge() {
            if(_allowDodging && IsGrounded()) {
                if(!_isDodging) {
                    if(_inputMovement != Vector2.zero) {
                        _animator.SetTrigger(_animDodge);

                        _isDodging = true;
                        yield return new WaitForSeconds(0.7f);
                        _isDodging = false;

                        _allowDodging = false;
                        yield return new WaitForSeconds(0.1f);
                        _allowDodging = true;
                    } else {
                        Debug.Log("BackStep Dodge");
                    }
                }
            }
        }
    }
#endregion

}