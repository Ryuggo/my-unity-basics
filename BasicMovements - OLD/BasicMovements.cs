using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace BasicLibrary.Movements {

    public class BasicMovements : MonoBehaviour {
#region Variables
        [Header("Camera")]
        [SerializeField] private bool _TPSOrFPS = true;
        [SerializeField] private bool _snappyMovements = false;
        [SerializeField] private Transform _camera;
        [SerializeField] private Transform _orientation;
        [SerializeField] private float turnSmoothTime = 0.1f;
        float turnSmoothVelocity;

        [Header("Ground Check")]
        [SerializeField] private Transform _groundCheckTransform;
        [SerializeField] private float _groundCheckRadius = 0.1f;
        [SerializeField] private LayerMask _groundLayerMask;



        [Header("General Movements")]
        [SerializeField] protected bool _moveInAir = true;
        protected Vector2 _inputMovement = Vector2.zero;
        protected Vector3 _cameraMovement;
        //protected Vector3 _prevMovement;

        [Space(10)]
        [SerializeField] private bool _holdWalk = false;
        [SerializeField] protected float _walkSpeed = 1.8f;
        protected bool _isWalking = false;

        [Space(10)]
        [SerializeField] protected float _jogSpeed = 4;

        [Space(10)]
        [SerializeField] private bool _allowSprinting = true;
        [SerializeField] protected float _sprintSpeed = 7.5f;
        [SerializeField] private bool _holdSprint = false;
        protected bool _isSprinting = false;

        [Header("Jumping")]
        [SerializeField] protected bool _allowJumping = true;
        [Tooltip("Default : 10 (with -30 in Project Gravity)")] [SerializeField] private float _jumpForce = 10;
        [SerializeField] private int _maxMultipleJumps = 0;
		[SerializeField] private float FallTimeout = 0.15f;
        private int _jumpsNumber;


        protected Rigidbody _rigidBody;
        protected InputMaster _inputMaster;
        [SerializeField] protected GameObject _player;
        protected Animator _animator;
        private float _fallTimeoutDelta;


    // Animations
		//private int string _animVelocityX = Animator.StringToHash("Velocity X");
		//private int string _animVelocityZ = Animator.StringToHash("Velocity Z");
		protected int _animMoveTrans = Animator.StringToHash("Move Transition");
		protected int _animJump = Animator.StringToHash("IsJumping");
		protected int _animGrounded = Animator.StringToHash("IsGrounded");
#endregion



#region Unity's Functions
        private void OnEnable() { _inputMaster.Enable(); }
        private void OnDisable() { _inputMaster.Disable(); }


        protected virtual void Awake() {
            _inputMaster = new InputMaster();
            _rigidBody = GetComponent<Rigidbody>();

            _animator = _player.GetComponent<Animator>();
        }
        protected virtual void Start() {
            // Details for me
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            Application.targetFrameRate = 120;

            InputEvent();
        }


        protected virtual void FixedUpdate() {
            // Prevent Sliding small slope      // Need to check Big slope so the slide still occurs
            if(IsGrounded()) {
                //_animator.SetBool(_animIDFreeFall, false);
				//_animator.SetBool(_animGrounded, true);

                //_animator.SetBool("grounded", true);
                if(_inputMovement == Vector2.zero && 0.1f < _rigidBody.velocity.magnitude && _rigidBody.velocity.magnitude < 0.5f)
                    _rigidBody.isKinematic = true;
            } else {
                /*
                if (_fallTimeoutDelta >= 0.0f)
					_fallTimeoutDelta -= Time.deltaTime;
				else
                    _animator.SetBool(_animIDFreeFall, true);

				_animator.SetBool(_animIDGrounded, false);
                //_animator.SetBool("grounded", false);
                */
            }

            Move();
        }
#endregion


        protected virtual void InputEvent() {
            // Subscribe to the Inputs Events
            _inputMaster.Gameplay.Walk.performed += ctx => OnMove(ctx);
            _inputMaster.Gameplay.Walk.canceled += ctx => OnStopMove(ctx);
            
            _inputMaster.Gameplay.Jump.performed += _ => OnJump();

            _inputMaster.Gameplay.Sprint.performed += _ => OnSprint();
            _inputMaster.Gameplay.Sprint.canceled += _ => OnStopSprint();

            _inputMaster.Gameplay.SlowWalk.performed += ctx => OnSlowWalk();
            _inputMaster.Gameplay.SlowWalk.canceled += _ => OnStopSlowWalk();

            _inputMaster.Gameplay.OpenMenus.performed += _ => Application.Quit();
        }

        protected bool IsGrounded() { return Physics.CheckSphere(_groundCheckTransform.position, _groundCheckRadius, _groundLayerMask); }
        protected virtual bool Momentum() { return (!IsGrounded() && (!_moveInAir || (_moveInAir && _inputMovement == Vector2.zero))) ? true : false; }
        protected virtual bool MoveOption() { return true; }
        protected virtual float AdjustSpeed() { return _isSprinting ? _sprintSpeed : !_isWalking ? _jogSpeed : _inputMovement == Vector2.zero ? 0.001f : _walkSpeed; }
        protected virtual void MoveTransitionAnim() { _animator.SetFloat(_animMoveTrans, _rigidBody.velocity.magnitude / (_isSprinting ? _sprintSpeed / 2 : !_isWalking ? _jogSpeed : _walkSpeed * 2)); }


#region Movements
        private void OnMove(InputAction.CallbackContext ctx) {
            _inputMovement = ctx.ReadValue<Vector2>();
            _rigidBody.isKinematic = false;

            // If Gamepad, detect Joging or Walking
            if(ctx.control.layout == "Stick") _isWalking = _inputMovement.magnitude <= 0.5 ? true : false;
        }
        private void OnStopMove(InputAction.CallbackContext ctx) {
            _inputMovement = Vector2.zero;
            _isSprinting = false;
            
            // If Gamepad was used, return to Joging
            if(ctx.control.layout == "Stick") _isWalking = false;
        }

        protected virtual void Move() {
            // Exit if not on the ground to keep the momentum
            if(Momentum()) return;
/*
            if(IsGrounded()) {
                // Animation
                _animator.SetFloat(_animMoveTrans, _rigidBody.velocity.magnitude / (_isSprinting ? _sprintSpeed / 2 : _moveSpeed));

                // Stop all Movements if no Inputs
                if(_inputMovement == Vector2.zero) {
                    _rigidBody.velocity = Vector3.zero;
                    _rigidBody.angularVelocity = Vector3.zero;
                    return;
                }
            } else
                if(!_moveInAir || (_moveInAir && _inputMovement == Vector2.zero))
                    return;
*/

            // Adjust directions depending of the camera position
            if(MoveOption()) _cameraMovement = _TPSOrFPS ? TPSCamera() : FPSCamera();

            // Change speed depending of the action
            float actualSpeed = AdjustSpeed();


            if(IsGrounded()) {
                // Animation
                MoveTransitionAnim();

                //_animator.SetFloat(_animMoveTrans, movements == Vector3.zero ? 0f :_isSprinting ? 2 : 1);
                //_animator.SetFloat(_animMoveTrans, Mathf.Sqrt(Mathf.Abs(movements.x + movements.z)) * (_isSprinting ? 2 : 1));

/*
                Vector3 animTransition =  _isSprinting ? movements * 2 : movements;
				_animator.SetFloat(_animVelocityX, animTransition.x);
                _animator.SetFloat(_animVelocityZ, animTransition.z);
*/

                // Counter Movements
                if(!_snappyMovements) CounterMovement(actualSpeed);
            }

            // Actual movements
            if(_snappyMovements)
                _rigidBody.velocity = new Vector3(_cameraMovement.x * actualSpeed, _rigidBody.velocity.y, _cameraMovement.z * actualSpeed);
            else {
                _rigidBody.AddForce(Vector3.forward * actualSpeed * _cameraMovement.z * 4 - _rigidBody.velocity);
                _rigidBody.AddForce(Vector3.right * actualSpeed * _cameraMovement.x * 4 - _rigidBody.velocity);
            }
        }

        private void CounterMovement(float actualSpeed) {
//_rigidBody.AddForce(new Vector3(-_rigidBody.velocity.x, 0, -_rigidBody.velocity.z) * (actualSpeed / 10));

//rb.AddForce(moveSpeed 4500 * orientation.transform.right * Time.deltaTime * -_cameraMovement.x * counterMovement 0.175);

//Debug.Log("TEST "+ actualSpeed);

            _rigidBody.AddForce(Vector3.forward / actualSpeed * -_cameraMovement.z * 1 - _rigidBody.velocity);
            _rigidBody.AddForce(Vector3.right / actualSpeed * -_cameraMovement.x * 1 - _rigidBody.velocity);


            /*
            if (Mathf.Abs(_cameraMovement.x) > 0.01f && Mathf.Abs(_inputMovement.x) < 0.05f || (_cameraMovement.x < -0.01f && _inputMovement.x > 0) || (_cameraMovement.x > 0.01f && _inputMovement.x < 0)) {
                //_rigidBody.AddForce(Vector3.right * actualSpeed * -_cameraMovement.x);
                _rigidBody.velocity = new Vector3(0, _rigidBody.velocity.y, _rigidBody.velocity.z);
            }
            if (Mathf.Abs(_cameraMovement.z) > 0.01f && Mathf.Abs(_inputMovement.y) < 0.05f || (_cameraMovement.z < -0.01f && _inputMovement.y > 0) || (_cameraMovement.z > 0.01f && _inputMovement.y < 0)) {
                //_rigidBody.AddForce(Vector3.forward * actualSpeed * -_cameraMovement.z);
                _rigidBody.velocity = new Vector3(_rigidBody.velocity.x, _rigidBody.velocity.y, 0);
            }
            */
/*

                if (_cameraMovement.x < _prevMovement.x - 0.5 && _prevMovement.x + 0.5 < _cameraMovement.x) {
                    //_rigidBody.AddForce(Vector3.right * -_cameraMovement.x * 2);
                    //_rigidBody.velocity = new Vector3(_rigidBody.velocity.x, _rigidBody.velocity.y, 0);
                }
                if(_cameraMovement.z < _prevMovement.z - 0.5 && _prevMovement.z + 0.5 < _cameraMovement.z) {
                    //_rigidBody.AddForce(Vector3.forward * -_cameraMovement.z * 2);
                    //_rigidBody.velocity = new Vector3(0, _rigidBody.velocity.y, _rigidBody.velocity.z);
                }


                if (_prevMovement.x != _cameraMovement.x || _prevMovement.z != _cameraMovement.z) {

                    //_rigidBody.drag = 10;
                    Debug.Log("HEAVY");
                } else {

                    //_rigidBody.drag = 0; // probably 0.0f, but it's always good to avoid those hard coded constants...
                }
                
            _prevMovement = _cameraMovement;
*/
        }


        private void OnJump() {
            if(_allowJumping) {
                _rigidBody.isKinematic = false;

                if(IsGrounded()) {
                    StartCoroutine(JumpAnim());
                    _jumpsNumber = 0;
                } 

                if(IsGrounded() || _jumpsNumber <= _maxMultipleJumps) {
                    _fallTimeoutDelta = FallTimeout;

                    Jump();
                    _jumpsNumber++;
                }
            }
        }
        protected virtual void Jump() {
            _rigidBody.velocity = new Vector3(_rigidBody.velocity.x, 0, _rigidBody.velocity.z);
            _rigidBody.AddForce(Vector3.up * _jumpForce, ForceMode.Impulse);
        }
        protected virtual IEnumerator JumpAnim() {
            //_animator.SetBool(_animGrounded, false);
            _animator.SetTrigger(_animJump);
            return null;
        }


        private void OnSprint() {
            if(_allowSprinting) {
                if(!_holdSprint)
                    _isSprinting = _isSprinting ? false : true;
                else
                    _isSprinting = true;

                SprintAnim();
            }
        }
        private void OnStopSprint() { if(_holdSprint) _isSprinting = false; }
        protected virtual void SprintAnim() {}


        private void OnSlowWalk() {
            if(!_holdWalk)
                _isWalking = _isWalking ? false : true;
            else
                _isWalking = true;
        }
        private void OnStopSlowWalk() { if(_holdWalk) _isWalking = false; }
#endregion


#region Cameras
        protected Vector3 TPSCamera() {
            Vector3 moveDirection = new Vector3(_inputMovement.x, 0f, _inputMovement.y).normalized;

            if(moveDirection.magnitude >= 0.1f) {
                float targetAngle = Mathf.Atan2(moveDirection.x, moveDirection.z) * Mathf.Rad2Deg + _camera.eulerAngles.y;
                float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);

                //if(18 < angle || angle < -18)
                transform.rotation = Quaternion.Euler(0f, angle, 0f);

                return Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
            } else
                return Vector3.zero;
            
        }
        protected Vector3 FPSCamera() {
            float lookAngle = _orientation.transform.eulerAngles.y;
            float moveAngle = Mathf.Atan2(_rigidBody.velocity.x, _rigidBody.velocity.z) * Mathf.Rad2Deg;

            float u = Mathf.DeltaAngle(lookAngle, moveAngle);
            float v = 90 - u;

            float magnitue = _rigidBody.velocity.magnitude;
            float yMag = magnitue * Mathf.Cos(u * Mathf.Deg2Rad);
            float xMag = magnitue * Mathf.Cos(v * Mathf.Deg2Rad);

            return new Vector3(xMag, _rigidBody.velocity.y, yMag);
        }
#endregion

    }
}