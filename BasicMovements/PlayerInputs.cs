using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

namespace BasicLibrary.Movements {

    public class PlayerInputs : MonoBehaviour {
#region Variable
        public PlayerManager playerManager;
        public InputMaster inputMaster;


        [Header("General Movements")]
        [SerializeField] private bool _holdWalk = false;

        [Space(10)]
        [SerializeField] private bool _allowSprinting = true;
        [SerializeField] private bool _holdSprint = false;

        [Header("Jumping")]
        [SerializeField] private bool _allowJumping = true;
        [SerializeField] private int _maxMultipleJumps = 0;
		//[SerializeField] private float FallTimeout = 0.15f;
        
        [Header("Dodging")]
        [SerializeField] private bool _allowDodging = true;
        
        [Header("Crouching")]
        [SerializeField] private bool _allowCrouching = true;
        public bool _holdCrouch = false;
#endregion



#region Unity's Functions
        private void OnEnable() { inputMaster.Enable(); }
        private void OnDisable() { inputMaster.Disable(); }

        protected virtual void Awake() {
            inputMaster = new InputMaster();
        }
        protected virtual void Start() {
            // Subscribe to the Inputs Events
            inputMaster.Gameplay.Walk.performed += ctx => OnMove(ctx);
            inputMaster.Gameplay.Walk.canceled += ctx => OnStopMove(ctx);

            inputMaster.Gameplay.Jump.performed += _ => OnJump();

            inputMaster.Gameplay.Sprint.performed += _ => OnSprint();
            inputMaster.Gameplay.Sprint.canceled += _ => OnStopSprint();

            inputMaster.Gameplay.SlowWalk.performed += ctx => OnSlowWalk();
            inputMaster.Gameplay.SlowWalk.canceled += _ => OnStopSlowWalk();

            inputMaster.Gameplay.Crouch.performed += _ => OnCrouch();
            inputMaster.Gameplay.Crouch.canceled += _ => OnStopCrouch();

            inputMaster.Gameplay.Dodge.performed += _ => StartCoroutine(OnDodge());

            inputMaster.Gameplay.Attack.performed += _ => StartCoroutine(OnAttack());
            inputMaster.Gameplay.Attack.canceled += _ => OnStopAttack();

            inputMaster.Gameplay.Block.performed += _ => OnBlock();
            inputMaster.Gameplay.Block.canceled += _ => OnStopBlock();

            inputMaster.Gameplay.OpenMenus.performed += _ => Application.Quit();
        }
#endregion


#region Events
        private void OnMove(InputAction.CallbackContext ctx) {
            playerManager.val.inputMovement = ctx.ReadValue<Vector2>();
            playerManager.val.rigidBody.isKinematic = false;
            if(playerManager.val.maxSpeed == 0) playerManager.val.maxSpeed = 4;

            // If Gamepad, detect Joging or Walking
            if(ctx.control.layout == "Stick") playerManager.val.isWalking = playerManager.val.inputMovement.magnitude <= 0.5 ? true : false;
        }
        private void OnStopMove(InputAction.CallbackContext ctx) {
            //playerManager.val.inputMovement = Vector2.zero;
            playerManager.val.maxSpeed = 0;
            //playerManager.val.isSprinting = false;
            
            // If Gamepad was used, return to Joging
            if(ctx.control.layout == "Stick") playerManager.val.isWalking = false;
        }


        private void OnJump() {
            if(_allowJumping) {
                playerManager.val.rigidBody.isKinematic = false;

                if(playerManager.val.grounded) {
                    playerManager.val.jumpsNumber = 0;

                    StartCoroutine(playerManager.script.JumpAnim());

                    playerManager.script.Jump();
                } else if(playerManager.val.jumpsNumber < _maxMultipleJumps)
                    playerManager.script.Jump();
            }
        }


        private void OnSprint() {
            if(_allowSprinting) {
                if(!_holdSprint)
                    playerManager.val.isSprinting = playerManager.val.isSprinting ? false : true;
                else
                    playerManager.val.isSprinting = true;

                if(playerManager.val.isSprinting) {
                    playerManager.anim.Crouch(playerManager.val.isCrouching = false);
                    playerManager.val.isWalking = false;
                }
            }
        }
        private void OnStopSprint() { if(_holdSprint) playerManager.val.isSprinting = false; }


        private void OnSlowWalk() {
            if(!playerManager.val.isCrouching) {
                if(!_holdWalk)
                    playerManager.val.isWalking = playerManager.val.isWalking ? false : true;
                else
                    playerManager.val.isWalking = true;
            }
        }
        private void OnStopSlowWalk() { if(_holdWalk) playerManager.val.isWalking = false; }


        private void OnCrouch() {
            if(playerManager.val.inputMovement == Vector2.zero && _allowCrouching) playerManager.script.Crouch();
        }
        protected void OnStopCrouch() {
            if(playerManager.val.inputMovement == Vector2.zero)
                if(_holdCrouch && playerManager.val.isCrouching) {
                    playerManager.val.isCrouching = false;

                    playerManager.anim.Crouch(false);
                }
        }


        protected virtual IEnumerator OnDodge() {
            if(_allowDodging && playerManager.val.grounded) {
                playerManager.anim.Attack(playerManager.val.isAttackingHold = playerManager.val.isAttacking = false);

                if(!playerManager.val.isDodging) {
                    playerManager.anim.Dodge();

                    playerManager.val.isDodging = true;
                    yield return new WaitForSeconds(35/30 - 0.1f);
                    playerManager.val.isDodging = false;

                    _allowDodging = false;
                    yield return new WaitForSeconds(0.1f);
                    _allowDodging = true;
                }
            }
        }


        protected virtual IEnumerator OnAttack() {
            if(playerManager.val.grounded && !playerManager.val.isAttacking) {
                playerManager.script.Attack();
                playerManager.anim.Attack(playerManager.val.isAttackingHold = true);

                playerManager.val.isAttacking = true;
                yield return new WaitForSeconds(17/30 - 0.1f);
                playerManager.val.isAttacking = false;
            }
        }
        public void OnStopAttack() {
            playerManager.anim.Attack(playerManager.val.isAttackingHold = false);
        }


        private void OnBlock() {
            if(playerManager.val.grounded) {
                playerManager.anim.Block(true);
            }
        }
        private void OnStopBlock() {
            if(playerManager.val.inputMovement != Vector2.zero) {
                playerManager.anim.Block(false);
            }
        }
#endregion

    }
}