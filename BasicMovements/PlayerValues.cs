using UnityEngine;

namespace BasicLibrary.Movements {

    [CreateAssetMenu(fileName = "PlayerValues", menuName = "Player Values")]
    public class PlayerValues : ScriptableObject {
#region Variables
        public Rigidbody rigidBody;
        public Animator animator;


        [Header("General Movements")]
        public float actualSpeed = 0;
        public float maxSpeed = 0;
        public float acceleration = 10;
        public float deceleration = 10;
        public bool moveInAir = true;
        public Vector2 inputMovement = Vector2.zero;
        public bool grounded = true;

        [Space(10)]
        public float walkSpeed = 1.8f;
        public bool isWalking = false;

        [Space(10)]
        public float jogSpeed = 4;

        [Space(10)]
        public float sprintSpeed = 7.5f;
        public bool isSprinting = false;

        [Header("Jumping")]
        public float jumpForce = 10;
		//public float fallTimeout = 0.15f;
        public int jumpsNumber;
        
        [Header("Dodging")]
        public float dodgeForce = 5;
        public bool canMoveWhenDodging = false;
        public bool isDodging = false;
        
        [Header("Crouching")]
        public float crouchSpeed = 3;
        public bool isCrouching = false;
        
        [Header("Attacking")]
        public bool isAttacking = false;
        public bool isAttackingHold = false;
#endregion
    }
}