using UnityEngine;

namespace BasicLibrary.Movements {

    public class PlayerCamera : MonoBehaviour {
#region Variables
        public PlayerManager playerManager;

        [Header("Camera")]
        [SerializeField] private Transform _camera;
        [SerializeField] private Transform _orientation;
        [SerializeField] private float turnSmoothTime = 0.1f;
        float turnSmoothVelocity;
#endregion



#region Camera
        public Vector3 TPSCamera() {
            Vector3 moveDirection = new Vector3(playerManager.val.inputMovement.x, 0f, playerManager.val.inputMovement.y).normalized;

            if(playerManager.val.actualSpeed >= 0.1f) {
                float targetAngle = Mathf.Atan2(moveDirection.x, moveDirection.z) * Mathf.Rad2Deg + _camera.eulerAngles.y;
                float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);

                //if(18 < angle || angle < -18)
                transform.rotation = Quaternion.Euler(0f, angle, 0f);

                return Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
            } else
                return Vector3.zero;
            
        }
        public Vector3 FPSCamera(Rigidbody rb) {
            float lookAngle = _orientation.transform.eulerAngles.y;
            float moveAngle = Mathf.Atan2(rb.velocity.x, rb.velocity.z) * Mathf.Rad2Deg;

            float u = Mathf.DeltaAngle(lookAngle, moveAngle);
            float v = 90 - u;

            float magnitue = rb.velocity.magnitude;
            float yMag = magnitue * Mathf.Cos(u * Mathf.Deg2Rad);
            float xMag = magnitue * Mathf.Cos(v * Mathf.Deg2Rad);

            return new Vector3(xMag, rb.velocity.y, yMag);
        }
#endregion

    }
}