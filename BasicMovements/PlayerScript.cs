using System.Collections;
using UnityEngine;

namespace BasicLibrary.Movements {

    public class PlayerScript : MonoBehaviour {
#region Variables
        public PlayerManager playerManager;

        [Header("Camera")]
        [SerializeField] private bool _TPSOrFPS = true;
        [SerializeField] private bool _snappyMovements = false;
        private Vector3 _cameraMovement;

        [Header("Ground Check")]
        [SerializeField] private Transform _groundCheckTransform;
        [SerializeField] private float _groundCheckRadius = 0.1f;
        [SerializeField] private LayerMask _groundLayerMask;
#endregion



#region Unity's Functions
        protected virtual void FixedUpdate() {
        //if(IsGrounded(Vector3.down)) _animator.SetBool(_animGrounded, true);
            // Prevent Sliding small slope      // Need to check Big slope so the slide still occurs
            if(playerManager.val.grounded) {
                //_animator.SetBool(_animIDFreeFall, false);
                playerManager.anim.Grounded(true);

                if(playerManager.val.inputMovement == Vector2.zero && 0.1f < playerManager.val.rigidBody.velocity.magnitude && playerManager.val.rigidBody.velocity.magnitude < 0.5f) playerManager.val.rigidBody.isKinematic = true;
            } else {
                /*
                if (_fallTimeoutDelta >= 0.0f)
					_fallTimeoutDelta -= Time.deltaTime;
				else
                    _animator.SetBool(_animIDFreeFall, true);

				_animator.SetBool(_animIDGrounded, false);
                */
            }

            IsGrounded();
            Move();

            if(playerManager.val.isAttackingHold && !playerManager.val.isAttacking) Attack();
        }
#endregion


        public void IsGrounded() { playerManager.val.grounded = Physics.CheckSphere(_groundCheckTransform.position, _groundCheckRadius + 0.2f, _groundLayerMask); }
        public virtual bool Momentum() { return (!playerManager.val.grounded && (!playerManager.val.moveInAir || (playerManager.val.moveInAir && playerManager.val.inputMovement == Vector2.zero))) ? true : false; }
        public virtual bool MoveOption() { return playerManager.val.isDodging && !playerManager.val.canMoveWhenDodging ? false : true; }
        public virtual float AdjustSpeed() { return playerManager.val.maxSpeed == 0 ? 0.0f : playerManager.val.isSprinting ? playerManager.val.sprintSpeed : playerManager.val.isDodging ? playerManager.val.dodgeForce : !playerManager.val.isWalking ? playerManager.val.jogSpeed : playerManager.val.isCrouching ? playerManager.val.crouchSpeed : playerManager.val.walkSpeed; }
        //public virtual float MoveTransitionAnim() { return playerManager.val.actualSpeed / 2;}


#region Movements
        private void Move() {
            // Exit if not on the ground to keep the momentum
            if(Momentum()) return;

            // Adjust directions depending of the camera position
            if(MoveOption()) _cameraMovement = _TPSOrFPS ? playerManager.cam.TPSCamera() : playerManager.cam.FPSCamera(playerManager.val.rigidBody);

            // Change speed depending of the action
            playerManager.val.maxSpeed = AdjustSpeed();

            // Smooth Acceleration / Deceleration
            if (playerManager.val.actualSpeed < playerManager.val.maxSpeed) playerManager.val.actualSpeed += playerManager.val.acceleration * Time.deltaTime;
            else playerManager.val.actualSpeed = playerManager.val.actualSpeed > -playerManager.val.deceleration * Time.deltaTime && playerManager.val.actualSpeed > 0.3 ? playerManager.val.actualSpeed -= playerManager.val.deceleration * Time.deltaTime : 0;

            if(playerManager.val.actualSpeed < playerManager.val.jogSpeed) playerManager.val.isSprinting = false;

//Debug.Log("Test " + playerManager.val.actualSpeed);

            // Exit if attacking
            if(playerManager.val.isAttacking || playerManager.val.isAttackingHold) {
                playerManager.val.rigidBody.velocity = Vector3.zero;
                //actualSpeed /= 4;
                return;
            }

            if(playerManager.val.grounded) {
                playerManager.anim.MoveTransition(playerManager.val.actualSpeed / 2);

                /*if(!_snappyMovements) {
                    playerManager.anim.VelocityX(playerManager.val.inputMovement.x * MoveTransitionAnim());
                    playerManager.anim.VelocityZ(playerManager.val.inputMovement.y * MoveTransitionAnim());
                }*/
            }
            

            // Actual movements
            //if(_snappyMovements)
                playerManager.val.rigidBody.velocity = new Vector3(_cameraMovement.x * playerManager.val.actualSpeed, playerManager.val.rigidBody.velocity.y, _cameraMovement.z * playerManager.val.actualSpeed);
            /*else {
                playerManager.val.rigidBody.AddForce(Vector3.forward * playerManager.val.actualSpeed * _cameraMovement.z * 4 * playerManager.val.speedMultiplier - playerManager.val.rigidBody.velocity);
                playerManager.val.rigidBody.AddForce(Vector3.right * playerManager.val.actualSpeed * _cameraMovement.x * 4 * playerManager.val.speedMultiplier - playerManager.val.rigidBody.velocity);
            }*/
        }


        public void Jump() {
            playerManager.val.rigidBody.velocity = new Vector3(playerManager.val.rigidBody.velocity.x, 0, playerManager.val.rigidBody.velocity.z);
            playerManager.val.rigidBody.AddForce(Vector3.up * playerManager.val.jumpForce, ForceMode.Impulse);

            playerManager.val.jumpsNumber++;
        }
        public IEnumerator JumpAnim() {
            playerManager.anim.Jump();
            yield return new WaitForSeconds(0.1f);
            playerManager.anim.Grounded(false);
            playerManager.anim.Crouch(playerManager.val.isCrouching = false);
        }


        public void Crouch() {
            if(!playerManager.inputs._holdCrouch)
                playerManager.val.isCrouching = playerManager.val.isCrouching ? false : true;
            else
                playerManager.val.isCrouching = true;

            playerManager.anim.Crouch(playerManager.val.isCrouching);
            if(playerManager.val.isCrouching) playerManager.val.isWalking = true;
        }


        public void Attack() {
            //playerManager.val.rigidBody.AddForce(Vector3.forward * _cameraMovement.z * 4, ForceMode.Impulse);
            //playerManager.val.rigidBody.AddForce(Vector3.right * _cameraMovement.x * 4, ForceMode.Impulse);
        }
#endregion

    }
}