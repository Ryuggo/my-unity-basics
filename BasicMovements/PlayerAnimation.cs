using UnityEngine;

namespace BasicLibrary.Movements {

    public class PlayerAnimation : MonoBehaviour {
#region Variables
        public PlayerManager playerManager;

        [Space(10)]
        public GameObject player;
        private Animator _animator;

        [Space(10)]
		private int _animVelocityX = Animator.StringToHash("Velocity X");
		private int _animVelocityZ = Animator.StringToHash("Velocity Z");
		private int _animMoveTrans = Animator.StringToHash("Move Transition");
		private int _animJump = Animator.StringToHash("IsJumping");
		private int _animGrounded = Animator.StringToHash("IsGrounded");
		private int _animCrouch = Animator.StringToHash("IsCrouching");
		private int _animDodge = Animator.StringToHash("IsDodging");
		private int _animAttack = Animator.StringToHash("IsAttacking");
		private int _animBlock = Animator.StringToHash("IsBlocking");
#endregion


#region Unity's Functions
        protected virtual void Awake() {
            _animator = player.GetComponent<Animator>();
        }
#endregion


#region Animations
		public void Grounded(bool b) { _animator.SetBool(_animGrounded, b); }
		public void Jump() { _animator.SetTrigger(_animJump); }
        public void Crouch(bool b) { _animator.SetBool(_animCrouch, b); }
		public void Dodge() { _animator.SetTrigger(_animDodge); }
		public void MoveTransition(float f) { _animator.SetFloat(_animMoveTrans, f, 0.1f, Time.deltaTime); }
		public void VelocityX(float f) { _animator.SetFloat(_animVelocityX, f); }
		public void VelocityZ(float f) { _animator.SetFloat(_animVelocityZ, f); }

		
		public void Attack(bool b) { _animator.SetBool(_animAttack, b); }
		public void Block(bool b) { _animator.SetBool(_animBlock, b); }
#endregion

    }
}