using UnityEngine;

namespace BasicLibrary.Movements {

    public class PlayerManager : MonoBehaviour {
#region Variables
        public PlayerValues val;

        [Space(10)]
        public PlayerInputs inputs;
        public PlayerScript script;
        public PlayerCamera cam;
        public PlayerAnimation anim;
#endregion


#region Unity's Functions
        protected virtual void Awake() {
            val.rigidBody = GetComponent<Rigidbody>();
        }
        protected virtual void Start() {
            // Details for me
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            Application.targetFrameRate = 120;
        }
#endregion
    }
}